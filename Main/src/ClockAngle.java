/**
 * A class to calculate angles based on the location of analog clock's hands.
 */
public class ClockAngle {


    /**
     * @param hours   an integer between 0 and 12 (inclusive)
     * @param minutes an integer between 0 and 59 (inclusive)
     * @return the smallest possible angle between the clocks hands
     * @throws IllegalArgumentException will be thrown, when inserted hours or minutes are not valid
     */
    public int calculateAngle(int hours, int minutes) {
        hours = convertTwelveToZero(hours);

        if (areHoursValid(hours) && areMinutesValid(minutes)) {
            int angle = Math.abs((int) calculateAngleForHours(hours, minutes) - calculateAngleForMinutes(minutes));
            angle = Math.min(360 - angle, angle);

            return angle;
        } else {
            throw new IllegalArgumentException("Please enter an hour in range 0 to 12 and minutes in range 0 to 59!");
        }
    }

    /**
     * @param hours   an integer between 0 and 12 (inclusive)
     * @param minutes an integer between 0 and 59 (inclusive)
     * @return an angle in degrees of the hand measured clockwise from 12
     */
    private double calculateAngleForHours(int hours, int minutes) {
        // The hour hand of an analogue clock turns 0.5° per minute.
        return (0.5 * (60 * hours + minutes));
    }

    /**
     * @param minutes an integer between 0 and 59 (inclusive)
     * @return an angle of hours relative to when minute hand is on 0
     */
    private int calculateAngleForMinutes(int minutes) {
        // The minute hand of an analogue clock turns 6° per minute.
        return 6 * minutes;
    }

    /**
     * @param hours an integer between 0 and 12 (inclusive)
     * @return whether the hours inserted are valid
     */
    private boolean areHoursValid(int hours) {
        return 0 <= hours && hours <= 12;
    }

    /**
     * @param minutes an integer between 0 and 59 (inclusive)
     * @return whether the minutes inserted are valid
     */
    private boolean areMinutesValid(int minutes) {
        return 0 <= minutes && minutes <= 59;
    }

    /**
     * @param hours an integer between 0 and 12 (inclusive)
     * @return hours from 12 converted to zero
     */
    private int convertTwelveToZero(int hours) {
        if (hours == 12) {
            hours = 0;
        }
        return hours;
    }
}
