public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException("Please insert two separate numbers!");
        }

        int hour;
        int minute;

        try {
            hour = Integer.parseInt(args[0]);
            minute = Integer.parseInt(args[1]);
        } catch (Exception e) {
            throw new IllegalArgumentException("Could not parse given arguments: " + args[0] + " " + args[1]);

        }
        ClockAngle clockAngle = new ClockAngle();
        System.out.println(clockAngle.calculateAngle(hour, minute));
    }
}
