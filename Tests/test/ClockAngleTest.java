import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClockAngleTest {

    private ClockAngle clockAngle;

    @BeforeEach
    public void setup() {
        clockAngle = new ClockAngle();
    }

    @Test
    public void calculateAngle_hours_are_0_and_minutes_are_0_should_return_0() {
        assertEquals(0, clockAngle.calculateAngle(0, 0));
    }

    @Test
    public void calculateAngle_hours_are_12_and_minutes_are_0_should_return_0() {
        assertEquals(0, clockAngle.calculateAngle(12, 0));
    }

    @Test
    public void calculateAngle_hours_are_3_and_minutes_are_0_should_return_90() {
        assertEquals(90, clockAngle.calculateAngle(3, 0));
    }

    @Test
    public void calculateAngle_hours_are_6_and_minutes_are_0_should_return_180() {
        assertEquals(180, clockAngle.calculateAngle(6, 0));
    }

    @Test
    public void calculateAngle_hours_are_1_and_minutes_are_50_should_return_115() {
        assertEquals(115, clockAngle.calculateAngle(1, 50));
    }

    @Test
    public void calculateAngle_hours_are_5_and_minutes_are_30_should_return_15() {
        assertEquals(15, clockAngle.calculateAngle(5, 30));
    }

    @Test
    public void calculateAngle_hours_are_4_and_minutes_are_44_should_return_122() {
        assertEquals(122, clockAngle.calculateAngle(4, 44));
    }

    @Test
    public void calculateAngle_hours_are_10_and_minutes_are_46_should_return_47() {
        assertEquals(47, clockAngle.calculateAngle(10, 46));
    }

    @Test
    public void calculateAngle_hours_are_13_and_minutes_are_7_should_return_IllegalArgumentException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            clockAngle.calculateAngle(13, 7);
        });
    }

    @Test
    public void calculateAngle_hours_are_7_and_minutes_are_92_should_return_IllegalArgumentException() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            clockAngle.calculateAngle(7, 92);
        });
    }
}